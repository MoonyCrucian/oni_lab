import math
import numpy

class Constants:
    T_start = 300
    T_end = 1000
    F_u0 = 1000.
    tau_u = 1.
    n_refract = 1.46
    R = 0.25
    R1 = 0.40
    h_x = 0.05
    h_phi = 0.1
    end_x = 1
    end_phi = math.pi
    alpha = 0.01
    tau = 1e-4
    delta = 0.2
    table_length = 0.
    temp_absorptance_table = numpy.array([[293, 0.002],
                              [1278, 0.005],
                              [1528, 0.0078],
                              [1677, 0.02]])
    ym = 0.
    phi_n = 0
    x_n = 0
    T_n = 0
    current_t = 0.
    sigma = 5.670373e-08
    def get_max_ns(self):
        self.phi_n = math.floor(self.end_phi/self.h_phi)
        self.x_n = math.floor(self.end_x/self.h_x)
        self.T_n = math.floor((self.T_end-self.T_start)/self.tau)
        self.table_length = self.x_n * self.phi_n

    def __init__(self):
        self.ym = math.tan(math.pi/2-self.delta)
        self.get_max_ns()

    def input_user_data(self):
        print("Начальное T:\t")
        self.T_start = input()
        print("Конечное T:\t")
        self.T_end = input()
        print("Шаг по T:\t")
        self.tau = input()
        print("R: \t")
        self.R = input()
        print("R1: \t")
        self.R1 = input()
        print("F_u0:\t")
        self.F_u0 = input()
        print("Максимальное x:\t")
        self.end_x = input()
        print("Максимальное phi:\t")
        self.end_phi = input()
        print("Шаг по х:\t")
        self.h_x = input()
        print("Шаг по phi:\t")
        self.h_phi = input()
        print("Alpha:\t")
        self.alpha = input()
        self.get_max_ns()


    def read_absorptance_from_file(self, filename):
        f = open ( filename , 'r')
        self.temp_absorptance_table = [ map(int,line.split(',')) for line in f ]