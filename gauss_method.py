import math
import numpy
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import gauss_elimination
from constants import Constants
import matplotlib.pyplot as plot

def T(t_ind, i, j):
    if (t_ind < 0):
        t_ind = 0
    return (T_list[t_ind, j*consts.x_n+i])


def find_parameters(t_ind):
    prev_t = t_ind - 1
    if (t_ind) == 0:
        prev_t = 0
    find_C_parameters(t_ind, prev_t)
    find_lambda_parameters(t_ind, prev_t)

def find_C_parameters(t_ind, prev_t):
    for i in range(0, consts.x_n):
        for j in range(0, consts.phi_n):
            C_grid[i, j] = 2.049 + 0.563*0.001*T(prev_t, i, j) \
                                  - (0.528*pow(10,5))/pow(T(prev_t, i, j), 2)

def find_lambda_parameters(t_ind, prev_t):
    for i in range(0, consts.x_n):
        for j in range(0, consts.phi_n):
            lambda_grid[i, j] = 0.134*0.1*(1.+4.35*0.0001*T(prev_t, i, j))


def find_equation_coeffs(t_ind):
    for j in range(0, consts.phi_n):
        for i in range(0, consts.x_n):
            if (j > 0):
                fA =  coef_grid[j*consts.x_n+i, j*consts.x_n+i-consts.x_n] = A(i, j, t_ind)

            if (i > 0):
                fb = coef_grid[j*consts.x_n+i, j*consts.x_n+i-1] = B(i, j, t_ind)

            fC = C(i, j, t_ind)
            coef_grid[j*consts.x_n+i, j*consts.x_n+i] = C(i, j, t_ind)

            if (i < consts.x_n-1):
                fD = coef_grid[j*consts.x_n+i, j*consts.x_n+i+1] = D(i, j, t_ind)

            if (j < consts.phi_n-1):
                fE = coef_grid[j*consts.x_n+i, j*consts.x_n+i+consts.x_n] = E(i, j, t_ind)
            coef_grid[j*consts.x_n+i, consts.table_length] = F(i, j, t_ind)



def a():
    t1 = (math.pi/2) - consts.delta
    t2 = 1 - (consts.R/consts.R1)
    return t1/t2

def x(i):
    return consts.h_x*i


def z(i):
    return 1 + (1/a())*math.atan((x(i)-1)*consts.ym)


def p(i):
    return (a()/consts.ym)*(1+math.pow(x(i)-1, 2)*math.pow(consts.ym, 2))


def A(i, j, t_ind):
    t1 = consts.h_x*consts.tau
    t2 = math.pow(consts.R1, 2)*p(i)*z(i)
    return (t1/t2)*lambda_j_half(i, j)

def lambda_j_half(i, j):
    l = lambda_grid.shape[1]
    if (j==0) or (j==l-1):
        return lambda_grid[i, j]
    if (j >= l):
        return lambda_grid[i, l-1]
    return (lambda_grid[i,j]+lambda_grid[i,j-1])/2.


def lambda_i_half(i, j):
    l = lambda_grid.shape[0]
    if (i==0) or (i==l-1):
        return lambda_grid[i, j]
    if (i >= l):
        return lambda_grid[l-1, j]
    return (lambda_grid[i,j]+lambda_grid[i-1,j])/2.


def B(i, j, t):
    t1 = consts.h_phi+consts.tau
    t2 = math.pow(consts.R1, 2)*consts.h_x
    return (t1/t2)*z(i-0.5)*p(i-0.5)*lambda_i_half(i, j)


def C(i, j, t):
    m1 = ( (z(i)*consts.h_x*consts.h_phi) / p(i) ) * C_grid[i, j]
    m2 = ( (consts.h_phi*consts.tau*z(i-0.5)*p(i-0.5)) / (consts.h_x*pow(consts.R1,2)) ) * lambda_i_half(i,j)
    m3 = ( (consts.h_phi*consts.tau*z(i+0.5)*p(i+0.5)) / (consts.h_x*pow(consts.R1, 2)) ) * lambda_i_half(i+1, j)
    m4 = ( (consts.h_x*consts.tau) / (pow(consts.R1, 2)*z(i)*p(i)) ) * lambda_j_half(i,j)
    m5 = ( (consts.h_x*consts.tau)/(pow(consts.R1, 2)*z(i)*p(i)) ) * lambda_j_half(i,j+1)
    return m1 + m2 + m3 + m4 + m5


def D(i, j, t):
    t1 = consts.h_phi*consts.tau*z(i+0.5)*p(i+0.5)
    t2 = consts.h_x * pow(consts.R1, 2)
    return (t1/t2)*lambda_grid[i,j]


def E(i, j, t):
    t1 = consts.h_x * consts.tau
    t2 = pow(consts.R1, 2) * z(i) * p(i)
    return (t1/t2)*lambda_j_half(i,j+1)



def k_absorp(t):
    t1 = consts.temp_absorptance_table[:,0]
    t2 = consts.temp_absorptance_table[:,1]
    if not (300 <= t <= 1677):
        t = 300
    res = interp_func(t)

    return res.tolist()


def q(i,j, t_ind):
    if (t_ind == 0):
        t = T(t_ind, i, j)
    else:
        t = T(t_ind-1, i, j)
    kp = k_absorp(t)
    exp_pow_f = consts.alpha*consts.current_t/consts.tau_u
    F = consts.F_u0 * consts.current_t * math.exp(-exp_pow_f)
    exp_pow_q = 0-(kp * (consts.R1 - consts.R))
    e1 = math.exp(exp_pow_q)
    t1 = pow(t, 4)
    temp2 = 4*kp*pow(consts.n_refract, 2) *consts.sigma*pow(t,4)
    q = consts.F_u0 * consts.R * kp * math.exp(-exp_pow_q) \
        - 4*kp*pow(consts.n_refract, 2) *consts.sigma*pow(t,4)
    return q


def F(i, j, t):
    m1 = ((z(i)*consts.h_x*consts.h_phi)/p(i)) * C_grid[i, j] * T(t-1, i, j)
    m2 = (z(i)*consts.h_x*consts.h_phi*consts.tau*q(i, j, t))
    return m1-m2


def main():
    t_ind = 0
    t = 0
    times = numpy.zeros((math.floor(consts.T_end/consts.tau), 1))

    while (t < 1e-2):
        consts.current_t = t
        find_parameters(t_ind)
        find_equation_coeffs(t_ind)
        r = gauss_elimination.gauss(coef_grid[:, :])
        if (t_ind == 0):
            numpy.savetxt('coefs_matrix.txt', coef_grid[:,:], '%3.5f', ' ')
        else:
            T_list[t_ind, :] = r[0, :]
        times[t_ind, 0] = t
        t_ind = t_ind + 1
        t = t + consts.tau
    #print(coef_grid)
    #print(T_list)


consts = Constants()
from scipy.interpolate import interp1d
interp_func = interp1d(consts.temp_absorptance_table[:,0], consts.temp_absorptance_table[:,1],
                    kind='cubic')
coef_grid = numpy.zeros((consts.table_length, consts.table_length+1))
T_list = numpy.zeros((consts.T_n, consts.table_length))
T_list[0, :] = consts.T_start
C_grid = numpy.zeros((consts.x_n, consts.phi_n))
lambda_grid = numpy.zeros((consts.x_n, consts.phi_n))
current_T = consts.T_start
T_ind = 0
if __name__ == "__main__":
    main()

